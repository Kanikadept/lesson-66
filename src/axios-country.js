import axios from "axios";

const axiosCountry = axios.create({
    baseURL: 'https://restcountries.eu/'
})

export default axiosCountry;
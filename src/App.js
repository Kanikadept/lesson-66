import React from "react";
import './App.css';
import Country from "./containers/Country/Country";

const App = () => (
    <div className="App">
        <Country />
    </div>
);

export default App;

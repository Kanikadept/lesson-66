import React, {useEffect, useState} from 'react';
import './CountryInfo.css';
import axiosCountry from "../../axios-country";
import withLoader from "../../hoc/withLoader/withLoader";


const CountryInfo = ({selectedCountry}) => {

    const [countryInfo, setCountryInfo] = useState(null);

    useEffect(() => {

        if(selectedCountry === null) {
            return null;
        }

        const getData = async () => {
            const countryResponse = await axiosCountry.get('rest/v2/alpha/' + selectedCountry);

            const promises = countryResponse.data.borders.map(async border => {
                const response = await axiosCountry.get('rest/v2/alpha/' + border);
                return response.data.name;
            });

            const newBorders = await Promise.all(promises);

            countryResponse.data.borders = newBorders;

            setCountryInfo(countryResponse.data);
        }
        getData();

    }, [selectedCountry])

    let countryDiv = null;
    if (!countryInfo) {
        countryDiv = <div>Choose country!</div>
    } else {
        countryDiv =
            <div>
                {
                    <>
                        <p>{countryInfo.name}</p>
                        <p><strong>Capital: </strong>{countryInfo.capital}</p>
                        <p><strong>Population: </strong>{countryInfo.population}</p>
                        <div className="borders">
                            <p>Borders with:</p>
                            <ul>{countryInfo.borders.map(border => {
                                return <li key={border}>{border}</li>
                            })}</ul>
                        </div>
                    </>
                }
            </div>
    }


    return (
        <div className="country-info">
            {countryDiv}
        </div>
    );
};

export default withLoader(CountryInfo, axiosCountry);
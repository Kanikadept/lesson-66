import React, {useEffect, useState} from 'react';

import './Country.css';
import ListOfCountries from "../ListOfCountries/ListOfCountries";
import CountryInfo from "../CountryInfo/CountryInfo";
import axiosCountry from "../../axios-country";

const Country = () => {

    const [countries, setCountries] = useState([]);

    const [selectedCountry, setSelectedCountry] = useState(null);


    useEffect(() => {

        const getData = async () => {
            const countriesResponse = await axiosCountry.get('rest/v2/all?fields=name%3Balpha3Code');
            setCountries(countriesResponse.data);
        }
        getData();

    }, [])


    const handleSelectCountry = (country) => {
        setSelectedCountry(country);
    }

    return (
        <div className="country">
            <ListOfCountries countries={countries} selectCountry={handleSelectCountry}/>
            <CountryInfo selectedCountry={selectedCountry} />
        </div>
    );
};

export default Country;
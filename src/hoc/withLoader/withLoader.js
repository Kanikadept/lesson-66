import React, {useState, useEffect, useMemo} from 'react';
import Spinner from "../../components/UI/Spinner/Spinner";

const withLoader = (WrappedComponent, axios) => {

    const WithLoader = (props) => {

        const [load, setLoad] = useState(false);

        useEffect(() => {

            axios.interceptors.request.use(req => {
                setLoad(true);
                return req;
            }, error => {

                throw error;
            });

            axios.interceptors.response.use(res => {
                setLoad(false);
                return res;
            }, error => {

                throw error;
            });

        }, [])

        return (
            <>
                {load && <Spinner/>}
                <WrappedComponent {...props}/>
            </>
        );
    }

    return WithLoader;


};

export default withLoader;